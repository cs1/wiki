Every so often the topic of adding a scripting language to Mutt comes
up. Most people don't understand the issues involved,
and why this hasn't been done so far. It's not a
matter of the developers being lazy, it's just a very difficult problem
to solve aesthetically.

Probably by far the biggest issue would be of choosing *which* scripting
language. S-Lang is often suggested since Mutt can
already be linked against it to use its screen management capabilities
in place of ncurses. The problem is that everyone has
their own favorite scripting language (eg. Perl, Python, TCL,
Scheme). If scripting support were ever to be added,
it would probably follow the model used by the GIMP,
with its procedural database (PDB) that allows you to write extensions
in any language, and make it available to any other
script no matter what language.

Engineering-wise, adding scripting to Mutt at this point is
difficult. If you look at other applications with
scripting support, primarily editors, they all operate on one basic mode
(a buffer). Mutt on the other hand has several
different modes--menus, pager. Not all functions can
be applied to every mode (consider Emacs: even when
you have different major modes, there is still a concept of a cursor-up
or cursor-down operation). So each intrinsic function
(the built-in functions available to scripts) would basically have to
keep track of what modes it is allowed to be called
from--messy. (ie, you can't call group-reply while in
the file browser because that wouldn't make sense).

For the most part, the keyboard macros in Mutt allow you to do most
everything you need to do. There are only a few
places where scripting support would really make
sense.

* prompt in the middle of a macro is impossible  
* applying a macro to all tagged messages  
* arbitrary editing of the message header in compose mode

Complex motions can be accomplished already, so they are not included
here. Some [ConfigTricks](ConfigTricks) might guide you in the right
direction, especially for "if" control.
