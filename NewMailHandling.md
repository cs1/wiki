This page collects ideas about a new mail detection/handling overhaul.

## New mail handling/reporting

### Supported workflows

There seem to be the following major workflows we want to
support:

 * a folder with new mail has new mail if and only if it hasn't been opened/closed by mutt since last new mail delivery  
 * a folder with new mail always has the new mail flag regardless if opened or not

From discussion it seems people use different ways to remind themselves
of things to be still looked at.

The idea behind the first approach is that once the user has visited the
mailbox, they know whether they want to deal with the unseen mail. If
they leave the mailbox with messages still unseen, they don't want to be
told about it again unless it has received new mail since that time.
Telling the user that there is new mail in the mailbox when it is
actually just unseen makes it impossible for the user to tell whether
really new mail has actually arrived.

For the latter, a mailbox-to-be-processed is one with a new mail flag.
Otherwise flagging or marking messages as old identify mail to be
defered for now because it makes the new mail flag for the mailbox
disappear. Due to being flagged/marked as old they can later still be
identified as to those needing to be looked at.

    #!comment
    does-not-exist is no longer a valid list archive. http://marc.info/?l=mutt-dev&r=1&w=2 is up as of 2013-10-21 but msg06522.html is hard to trace atm 

There's a nice old summary of the difference between mbox, maildir, and
IMAP in [mutt-dev
\<20060529120134.GA23719@cube.chello.upc.cz\>](http://does-not-exist.org/mail-archives/mutt-dev/msg06522.html).
See [this
thread](http://does-not-exist.org/mail-archives/mutt-dev/msg13177.html)
for discussion as of mutt 1.5.20.

It doesn't make any sense to have mutt announce that there's new mail in
a mailbox you've just left. But it can make sense for it to appear in
the buffy list.

### Status

Starting with 1.5.20 we do the latter for mbox as well as maildir (but
not IMAP). However, many mbox users seem to miss the old behaviour and
consider the new one an outright bug. This is __exactly__ the
opposite for maildir/mh users.

### Solution

We could let `$mark_old` affect polling behavior: when set, mutt will
not only mark unseen messages as *old* when leaving the mailbox, but it
will also skip mailboxes that have only *old* messages in the list of
mailboxes with new mail. Users that use `$mark_old` are most likely
to want this behavior, as it is probably why they are using the *old*
flag in the first place. However, it may be that there are people who do
not want an Old flag on their messages but do want to skip mailboxes
that mutt has already visited. This would require a separate flag,
perhaps `$show_unseen` which would show mailboxes with unseen
messages as well as new messages.

An option should be added that toggles between both; not sure about a
default value as one group or the other will get changed behaviour by
default. Inspecting `$mbox_type` sounds wrong.

For mbox/mmdf, the change would trivial, simply avoid looking for a new
message in `mbox_reset_atime()`.

For maildir, it could be implemented by changing the buffy check to
report new mail only if we have a message with a mtime greater than the
last check time in `new/`. The drawback is that this has the potential
to slow things down depending on how many messages we have in `new/`
and many "seen" messages we need to skip (usually the first hit using
`readdir()` reveals a non-trashed one so we can abort). Or we can use
the mtime of `new/` subdirectory if that's reliable enough (i.e. don't
start looking if it's `<=` last time checked).

## Appending new mail

### Current situation

New mail handling is different is when appending a new message to a
folder from within mutt: For mbox/mmdf it doesn't raise the new mail
flag, for maildir/mh it does.

### Solution

This is not trivially solved as the mbox/mmdf driver doesn't care about
what the other code appends to the folder (possible some copy routine).
But it would need to inspect it in order to tell whether the there's at
least one new message for the new mail flag. The easiest way was if we
had an append method that passes the required info from HEADER down to
mx driver. Right now, appending to mbox is cheap as we simply seek to
the end and return the FILE pointer for appending (i.e. the mbox code
itself doesn't know whether the message is new or not). Maybe we need
something like `mbox_commit_message()` as other driver have (which
would have the benefit of a cleaner mx API).

This is likely to trigger some complaints by users, too, I think because
it changes well-known behavior for mbox.

## FCC handling

This is consistent with the fix in \[7d7976cd4fc4\]: as FCC messages are
known to the author, they're aren't new and thus shouldn't raise the new
mail flag. However, there's at least one report in
`<20090701212335.GA3857@cefeid.wcss.wroc.pl>` requesting the old
behavior for mbox/mmdf. See ticket \#1896.

## Postponed messages

There's a request in ticket \#3295 to not mark postponed mail as new.
This probably only matters when the user configures mutt to watch the
postponed folder (watching it may make sense with an IMAP account that
has a draft folder the user is subscribed to). One argument for marking
postponed mail as read is that the message isn't actually unread but
well known to the user.

## Deadline

All this should be solved for 1.6.

## Workaround

For people who keep new messages in their mailboxes (thus, having
$mark_old unset), `set check_mbox_size=yes` might be a working
workaround to bring back the old behavior.
