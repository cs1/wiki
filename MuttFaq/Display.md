### How do I change the format/ display of the list of mailboxes/ folders (or any menu)?

The default display of mailbox folders includes a lot more information
than sometimes needed. It is "N permissions owner group size date name"
but you might prefer something shorter like "N size date name". You need
to adjust the **$folder\_format** variable to configure the look of the
display of available inboxes (**browser**), look it up in the
manual.txt. In general, you customize views with the \*\_format set of
variables, there are some of
them.

`set folder_format="%2C %t %N %8s %d %f"  # what I settled on`

### Can I personalize/ shortcut the display of or label the folder names in the folder browser?

No. :-)

Either try your luck with /RemoteFolder hints for shortening pathnames
via **$folder**, or name the folders as needed (for sorting or saving
typing).

### Why doesn't $index\_format "%b" show some Maildir?

Because "%b" extracts the string after the last slash of the mailbox. If
you want to see the string "mail" for "%b" on "/home/you/mail" Maildir,
you have to go to "/home/you/mail", i.e. you should not go to
"/home/you/mail/" (there is nothing after the last slash).

Mutt has also a similar bug. Mutt's directory browser does not allow you
to go to ".." when $folder is "/home/you/mail/". This is bug\#1885.

Conclusion: Be careful not to add "/" after Maildir (or MH) folders.

### Can Mutt show the (new) message count in the folder browser?

It depends on the folder type:

  - IMAP - yes, but not all servers support it
  - mbox - no
  - maildir - no

See "folder\_format" for details, %N will give the number if supported.
For maildir, you can try the "sidebar" patch from the [PatchList](PatchList). It
isn't supported by the Mutt developers,
though.

### How to make the From-addr shown normally in index-view to show To-addr for stuff that I sent?

You have to setup the addresses by which mutt should identify you as
sender by using "alternates" and see also "index\_format" %-codes.

### Can mutt use virtual folders/ mailboxes?

No, not directly.

The best you can get only using mutt is defining "mailboxes" for folders
receiving new mail, and have those listed in the folder-browser (with
indication of which one has new mail: see folder\_format) by pressing
"c?TAB" (by default, otherwise "c??" and search for "toggle-mailboxes").

External tools such as mairix or nmzmail can be used for this purpose.
With them, you can create search patterns (you can view them as filters)
that will create copies/symlinks of your messages into a mailbox of your
choice. See
UserStory/SearchingMail

### Why are some 'N'(ew) messages flagged 'O'(ld) when I leave the folder containing them and return?

When you read a msg, mutt removes the "N"ew flag from it. When you quit
mutt, it can happen that you haven't read \_all\_ new msgs. mutt will
mark those "seen-but-not-read" as "O"ld for the next run, to remind you
that they arrived already in the previous session but you haven't read
them yet. Old == New (unread) from last session. "**mark\_old**"
controls wether to distinguish "O" from "N" or not.
