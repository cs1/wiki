### How do I set my From: address?

### How do I set mutt to allow editing of the From: header during composition?

The primary way to set your default sender address is through the
**$from** and **$realname** variables in muttrc:

``` 
 set realname="Joe User"
 set from="user@host"
 set use_from=yes
```

**NOTE:** Another way is to use **my\_hdr From:** but it is
discouraged because it has drawbacks: It breaks the
**$reverse\_name** feature, and doesn't work for messages resent
using the \<**bounce-message**\> function. While **my\_hdr From:**
can be a convenient temporary override of the default address, it is
not as convenient for setting the default address.

To instruct mutt to show the From: header during composition,
use:

``` 
  set edit_headers = yes
```

### How do I set the address used in the SMTP negotiation (envelope address)?

### I've set my From: header field in Mutt, but the SMTP server still sees my local hostname.

### Remote SMTP server refuses to accept my eMail because of wrong domainname, but I gave correct "From:"?

Put

``` 
  set use_envelope_from = yes
```

in your *~/.muttrc*. This adds the *-f* option when calling
**$sendmail** to deliver the mail, forcing it to use the same address
for the envelope as for the From: header field.

''Your [MTA]([MailConcept](MailConcept) "wikilink") must understand this parameter &
functionality!

Note: **use\_envelope\_from** was formerly called **envelope\_from**

Without **set use\_envelope\_from=yes** your ISP may bounce messages
with: 'status=bounced' and '501 sender domain must resolve (in reply to
MAIL FROM command)'.

But sometimes this isn't good enough. For example, on some system with
an out-of-the-box sendmail configuration, this technique causes the
following field to be added to the message
header:

``` 
 X-Authentication-Warning: <HOSTNAME>: <USERNAME> set sender to <WHAT YOU WANTED> using -f
```

So, your identity (username) is revealed anyway. The way around this
depends on your MTA:

  - sendmail users can add their username to the list of *trusted* users
    in **sendmail.cf**, or google for the
    "FEATURE(masquerade\_envelope)dnl".

<!-- end list -->

  - Exim supports a file like /etc/email-addresses where you can relate
    users to their correct adresses (which is handy when using a
    smarthost).

### I have several different roles (From:-addresses), how to manage them?

One method is to use different mutt config files by calling mutt using
the **-F** flag to tell it what profile to use. A convenient way to do
it is to create, say, a directory **~/.mutt** to hold all profile
configuration.

For example, for a profile p1 you can create a file **~/.mutt/p1**, and
put all the profile specific set lines in it:

``` 
 set pager_format="(P1) %S [%C/%T] %n (%l) %s"
 set realname="Joe Doe"
 set from=p1@example.COM
 my_hdr Organization: P1's Organization
 my_hdr PGP: s
 set pgp_sign_as="keyid"
 set signature="~/.signature-p1"
 color status black p1-color
```

It is convenient to indicate the profile with specific colours and
statusbars (like **pager\_format** in this example).

All the other settings can be put in another file and then read into
each profile using **source**. Be careful to remove all account-specific
information from that file.

``` 
 source ~/.mutt/common
```

To avoid the tediousness of having to type **mutt -F ~/.mutt/p1** every
time, add shell aliases like the one below:

``` 
 alias mutt-p1="mutt -F $HOME/.mutt/muttrc-p1"
```

Another way is creative use of account-hook: and the various other hooks
available.

`XXX someone come up with an example`

### How to let mutt use the To: address of a message as the From: address in a reply?

A simple way is to use the following three commands in ~/.muttrc:

``` 
 set reverse_name  
 set from=default@example.COM
 alternates "alt1@example.COM|alt2@example.NET"
```

Do not have a **my\_hdr From:** active, as this would overrule
**$reverse\_name**.

For further information check [manual.txt]([MuttWiki](home) "wikilink") for
setting up *automatic* reactions by these:

  - variables: **$from**, **$realname**, **$reverse\_name**, and
    **$reverse\_realname**.
  - cmds: **folder-hook**, **message-hook**, **reply-hook** (v1.5.x),
    **send-hook**, and **my\_hdr**.
  - additionally: **alternates** (a cmd after 1.5.6, a variable up to
    1.5.5).

To change it *manually* (on demand, by key press), **macro**s can be of
great use for this, too. Just using **$reverse\_name** is enough in many
cases, try this first.

Note it is generally better to manipulate **$from**, **$realname**, and
**$reverse\_name**. The **my\_hdr From:** override should better be
reserved for only where it is necessary: In **send-hook**s. It can be
useful in **message-hook**s and **macro**s, though, for its *temporary
override* nature. But handle with care, trying to avoid as much as
possible its drawbacks.

If additionally you have to use different SMTP-servers depending on the
From-addr, then have a look at /Sendmail, too.

For example, if you want send an email with a different From: Name
<email> to one list, you can add this to your .muttrc:

``` 
 send-hook .               "unmy_hdr From:"
 send-hook name-of-list    "my_hdr From: List Name <new email>"
 send-hook @company\\.tld$ "my_hdr From: Company Name <new email>"
```

Note that the **unmy\_hdr From:** in default **send-hook** placed first
permits to fallback to the normal default "From:" field for mails not
sent to the mailing list or company. Please check [MuttFaq](MuttFaq) & [MuttGuide](MuttGuide)
for "hook"-traps, and more detailed examples of multiple profiles.

### I set $reverse\_name, but it's not working!

The original message (the one to which you are replying) must have been
sent to an address which matches **$alternates** (v\<=1.5.5) or
**alternates**-cmd (v\>=1.5.6). Also there must be no **my\_hdr From:**
active: It would overrule **$reverse\_name**. See the previous question
for a way to do it.

### How can I change "Message Priority" for outgoing mails?

How can I add something like "Low, Normal, Medium, High" priority to a
mail right before sending?

This is usually done through a **Priority:** header, although it is not
a standard header. It is up to the [MUA]([MailConcept](MailConcept) "wikilink") whether
it supports it or not. If the receiving MUA does not support it, it will
not be displayed to the receiver. Mutt allows you to setup **macros**,
various -hooks, and **my\_hdr** to add the header to a message.
