## UserStory/MultiAccounts

This short guide might be useful if you have more than one address and
would like mutt to automatically choose the right one when answering
e-mail.

### Essential stuff: reverse

You will need to set this variable in order for mutt to build the From:
line of your replies using the address where you received the messages
you are replying to.

``` 
 set reverse_name=yes
```

This will only work if your address matches your alternates. (see next
section)

**Important:** As [MuttFaq/Header](MuttFaq/Header) says, there must be no "my\_hdr From:"
active: It would overrule "$reverse\_name" and render the above setting
useless. If you configured your default From: address using a send-hook,
you will need to change this. Please see below.

If you want to also include the real names the person that e-mailed you
used, you need to use this.

``` 
 set reverse_realname=yes
```

**Note:** This var is set to 'yes' by default, so remember to change it
if you do not want to use those names.

Also verify that the **$use\_from** variable is set to "yes". It is
correct by default, but may be unset say in Debian's global
`/etc/Muttrc`.

### Setting up the addresses: alternates

The variable that holds the different addresses mutt can use is called
'alternates'. Its syntax was changed in mutt 1.5.6, so most of the
sample configuration files you'll find might look like this:

``` 
 set alternates="^(john@example.COM|john@example.ORG)$"
```

When in reality, it will only work if you type it like this:

``` 
 alternates     ^john@example.COM$     ^john@example.NET$
```

Please note that you can't add a "real name" to the alternates variable.
This is for addresses only. So if you set reverse\_realname to 'no' and
want to specify your realnames, see the next section to find out how to
deal with them.

### Real names: send-hooks

If you want to use different realnames for your different accounts, you
can set a send-hook for each of them. Here's an example:

``` 
 send-hook     .                          'set realname="John Doe"'
 send-hook     "~f ^john@example.COM$"     'set realname="Johnny"'
```

### Default account for new messages

Up until now, you can reply to messages and mutt will automatically
select the appropriate *From:* header based on the message you are
answering *To:* header. For when composing a new message, declare
statically default **$from** and **$realname**:

``` 
 set from=john@example.COM
 set realname="John Doe"
```

In addition to this static setup, you can change **$from** and
**$realname** at will, perhaps in **folder-hook**s or **macro**s.

### Specific accounts for specific recipients

If you want to override the whole scheme described on this page, and
write to some recipients always with a fixed *From:* field, whatever new
mail or reply, you can use:

``` 
 send-hook     .                       "unmy_hdr From:"
 send-hook     ^emm@example.COM$     "my_hdr From: agent 007 <john@example.COM>"
```

This override has a weakness: Mails resent to those recipients with the
**\<bounce-message\>** function (bound to the \[b\] key by default) will
be *Resent-From:* your default account, instead of the expected fixed
account.
