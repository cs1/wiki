## How to debug mutt config problems

The basic
principle:

* **Start** with a **simple** config that works,  
 * Use `mutt -n` to exclude side-effects of a global Muttrc.  
 * Use `mutt -F file` for a temporary config-file. (both can be combined)  
* then expand it **step by
step** with more of your config lines, limit your changes related to only 1 problem at a time: **isolate,
eliminate**.  
* If you have a big file and can't find the breaking point, 1st drop one half, then the other, repeat in the broken half: **divide + conquer**.  
* until you find the error or your config is complete.

The gory
details:

* Always check whether mutt-vars have the desired values by entering "`:set ?var`" at run-time.  
 * TAB-expand mutt varnames until complete, expand their values by adding "`=`" after the name and hitting TAB again, i.e. without the leading '`?`'.  
* Simplify or even disable hooks. Read carefully which applies for which task, don't mix 'em up.  
 * Verify that things work without hooks first.  
* Take care of matching quoting chars/ levels -> [PatternQuoting](PatternQuoting)!  
* When (error-)messages pass too fast, increase "sleep_time".  
* Compile with "debug" enabled, then run "`mutt -d 3 ...`" to watch output in `~/.muttdebug...` 

Interaction with other
parts:

* Use shell-scripts in place of external application calls to see whether the app or mutt produces an error and why:  
 * make executable script ("`man chmod`"): `{ echo "Args = $@"; echo "STDIN blow: ======"; cat ; } > /tmp/mutt-interface`   
 * inspect that tmp-file to check arguments and input are OK from mutt,  
 * pass examined data caught by script manually to external application on the cmd-line,  
 * verify output and exitcode of app supposed to be returned to mutt are OK.

Config not portable across
systems:

* employ the logfile feature described above to see what happens, also dump mutt-vars with "`mutt -D`", (defaults may vary)  
* take care to reproduce the execution of functions **identically** on both systems and then compare the outputs with "`diff`".  
* If the logfile and "`mutt -D`" output is identical, then the system-environments differ in vital details;  
 * which, however, vary by features too much to list them all here: see [MuttFaq](MuttFaq).

-----

## Typical problems

* Misunderstanding of how hooks work:  
 * they always need a default ("catch-all") pattern/ regex == '`.`'!  
 * their order matters and varies by type (especially where defaults go, when "`:set ?var`" fails)!  
  * hooks with filename as result take the **first
hit** exclusively (put default as last hook),  
  * mutt-cmds as result means **all** hits are applied cumulatively, at conflict last hook wins (put default as first hook).  
 * cmds in hooks are executed when triggered, therefore errors, too, occur later, not at startup.  
 * see `send-hook` vs. `send2-hook`, maybe the latter can help where the former can't.  
 * `folder-hook` for doing stuff *after **changing**
folders*, everytime,  
 * `account-hook` to setup account-stuff (like *_user&*_pass) *before
**connecting** to a
site*, once at login.  
  * changing folders doesn't require new connections (stay open in background).  
* Quoting spaces and regexps/ patterns on several levels of nested cmds as args to cmds:  
 * "cmd-arg" to hook/ macro must be quoted completely, since hook/ macro takes only 1 token as whole (incl. args of the cmd):  
  * if you want to specify more than 1 cmd as arg, the separating "`;`" must be quoted in the cmd-part of the hook/ macro.  
   * example double quoting with '""': `hook 'pattern' 'cmd "param"; cmd2 "param2"'`   
 * see [MuttGuide/Syntax](MuttGuide/Syntax) for what is quoted when and how.  
 * [PatternQuoting](PatternQuoting) explains how to cope with 2 levels and patterns.  
  * for each nested lvl of cmds as args to cmds you must add an extra lvl of quoting **for
that
lvl**: not all chars are special on every lvl.  
* Side-effects by other config files:  
 * a global Muttrc with bad defaults for all users: "`mutt -v | grep SYSCONFDIR`" to find it.  
 * user files sourced by macros or hooks possibly overwriting vars, when the "`:set ?var`" check fails.  
* Side-effects by 3rd party patches: retry with **native
mutt** before asking the wrong people, ask patch provider if it's patch-related.  
* Features not enabled in binary: see [MuttGuide/Setup](MuttGuide/Setup) to figure out which are enabled and whether you need to build a new binary.  
* Special treatment of '\' line continuation with '#' comments per [MuttGuide/Syntax](MuttGuide/Syntax):  
 * if you comment lines, make sure you hit the beginning and don't break or oversee \-continuations, especially with hooks.  
* Macros badly defined:  
 * use "`<>`" notation for functions rather than keystrikes, allows to better detect typos and more portable.  
 * remember: macros are a _replay_ of keystrikes you'd normally type manually: always check whether the sequence works manually with **each
key**!  
* Confusion about use of "`set from=...`" and "`my_hdr`", see [MuttFaq/Header](MuttFaq/Header).  
 * "`my_hdr`" has some issues when interacting with "`send-hook`" for example, read docs about both.

-----

Sometimes the problem is not the config but the code: update to the most
recent developer version and retry. This way you exclude problems with
your old version that might have been fixed in the meantime. See also
reported errors in the bug tracking system.

-----

Always remember to have a look at [MuttFaq](MuttFaq) if you get stuck somewhere.
