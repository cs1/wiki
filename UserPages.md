This page lists webpages set up by Mutt users, containing various
helpful hints, configs and patches. However, there exist dedicated pages
for [ConfigList](ConfigList), [QueryCommand](QueryCommand), [PatchList](PatchList), [MuttTools](MuttTools), [CheatSheets](CheatSheets), and
quick-start
[MuttGuide](MuttGuide).

* <http://mutt.justpickone.org/> -- David T-G  
 * mutt-rpm -- Mutt RPMs  
 * mutt-build-cocktail -- scripts to build Mutt [mostly Mutt 1.3]  
* <http://cedricduval.free.fr/mutt/> -- Cedric Duval  
 * French translation of the official manual  
* <http://www.spinnaker.de/mutt/> -- Roland Rosenfeld  
 * lbdb -- little brothers database  
 * metamutt -- metamail-like MIME viewer  
* <http://www.wolfermann.org/mutt.html> -- Armin Wolfermann  
 * abook-autoexport -- wrapper script to keep abook database and Mutt's aliases synchronized  
* <http://www.davep.org/mutt/> -- Dave Pearson  
 * neat hack to handle octet-stream MIME attachments, Emacs-Lisp script for alias handling, script to convert ~/.muttrc into HTML, VCard filter  
* <http://www.spocom.com/users/gjohnson/mutt/> -- Gary Johnson  
 * lots of scripts for MIME handling, especially for MS-Office documents, tips for using w3m, hiding long To: and Cc: lines, setting message expiry dates  
* <http://mutt-j.sourceforge.jp/> -- Mutt Users in Japan  
 * Mutt-J -- has many links to Japanese users  
* <http://vicerveza.homeunix.net/~viric/doc/linukso-lingva-howto> -- Lluís Batlle i Rossell   
 * Howto about how to use unicode in Xfree86 4.3, enabling Xfree to understand more than two keyboards.  
 * The text is written in Esperanto, as it is for users of Esperanto. The goal is use mutt and vim in unicode.  
  * "Kiel fari" dokumento pri unikodigi sistemon Xfree86 versio 4.3, ebligante tajpi per pli ol tiuj du Xfree-aj klavaroj (t.e. pli ol angla+alia lingvoj).  
  * La dokumento estas skribita esperante. La fina celo estas uzi mutt kaj vim unikode.  
* <http://www.hollenback.net/index.php/HelpfulMuttHints>  
 * Sample config file entries, mutt helper scripts, and my hacked post.el mutt editor mode for Xemacs  
* <http://www.panix.com/~rbean/procmail/>  
 * Using mutt with procmail to build a semi-automatic whitelist (exceptions in your spam filter for people you've sent mail to).  
 * This requires some knowledge of procmail as well as mutt. Includes links to additional resources.  
* <http://temporaer.livejournal.com/tag/mutt>  
 * Automatically add greetings like "Hello Mr. X" to new mails/answers (uses vim as editor)  
 * Automatically sort mails into folders according to mutt addressbook (uses procmail)  
* <http://linsec.ca/Using_mutt_on_OS_X>  
 * How to setup mutt on OS X from beginning to end: examples and instructions for procmail, fetchmail, lbdb, urlview, mailcap, exim, postfix, and most importantly mutt itself  
* <http://foolab.org/node/1737>  
 * How to use mutt to convert between different mailbox types.  
* <http://kenai.com/projects/sidebar-config/>  
 * Python scripts to generate sidebar config based on IMAP or maildir folders.
