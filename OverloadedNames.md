# Overloaded Names in Mutt, Procmail and the Rest of the Mail World

Unfortunately, there are several terms that have multiple meanings. This
page aims to explain the proper usage of terms.

Overview table:

|       Desc              | Mutt        | procmail                          | common setting               |
| ----------------------- | ----------- | --------------------------------- | ---------------------------- |
| default folder location | $folder     | $MAILDIR                          | ~/Mail                       | 
| default folder format   | $mbox\_type | mbox (trailing slash for maildir) | mbox or maildir              |
| read mail folder        | $mbox       | N/A                               | ~/mbox                       |
| inbox location          | $spoolfile  | $DEFAULT                          | /var/mail/$USER or ~/Maildir |

## folder

1. **A folder stores
mail.** A folder is a thing that contains messages.  
 A folder can be anything from a single file (mbox format),  
 a directory with files (MH format, deprecated),  
 a directory with subdirectories with files (maildir format),  
 or even a remote resource on the network (IMAP and folders; POP only supports one folder per account, the inbox).  
1. **"folder" is a directory that contains
folders.**  
 This meaning is used by Mutt's $folder variable.  
 There's nothing special about that directory, it is just what you get when you open =fldr (or equivalently +fldr), which will expand to $folder/fldr.  
 Mutt defaults to ~/Mail; ~/mail is also common. ($folder roughly corresponds to procmail's $MAILDIR.)

## mbox

1. **mbox is a folder
format.** The mbox format stores all messages in a single big file, separated by "From " lines (sometimes called From_ lines).  
 mbox is the default format used by most MTAs for /var/mail/$USER. Alternatives are the [MaildirFormat](MaildirFormat) (see below) and MH (deprecated).  
1. **mbox is a folder that stores read
mail.**  
 With move=(ask-)yes, Mutt moves read mail from your $spoolfile to $mbox (which defaults to ~/mbox) when exiting mutt (or leaving the $spoolfile folder).  
1. **Mutt's default folder format is
$mbox_type.** Mutt auto-detects formats when opening folders; $mbox_type is used when creating new folders.

## maildir

1. **maildir is a folder
format.** The [MaildirFormat](MaildirFormat) stores messages in separate files in subdirectories new, cur, and tmp below a common parent directory.  
 In procmail, a trailing slash makes a folder use [MaildirFormat](MaildirFormat) (fldr/; Mutt does not care about trailing slashes).  
1. **maildir is a folder that gets incoming
mail.** When the $spoolfile (inbox) is not in /var/mail but in the user's home, it is often in ~/Maildir,  
 which incidentally uses the [MaildirFormat](MaildirFormat). (Courier [?] introduced that location, thereby also giving the format its name.)  
1. **maildir is a directory that stores
folders.** This is the meaning procmail uses. When assigning MAILDIR=some/dir,  
 procmail chdirs to that directory and all subsequent references to fldr will now write to $HOME/some/dir/fldr (instead of $HOME/fldr before). 

## mailbox(es)

1. **mailbox is a synonym for
"folder".** Sometimes, people refer to folders as mailboxes.  
 In a stricter meaning, the mailbox is the inbox where new mail gets delivered ($spoolfile in Mutt, $DEFAULT in procmail.)  
1. **mailboxes are what Mutt checks for new
mail.** Mutt periodically checks any folder declared as "mailboxes" (plural!) in .muttrc for new mail  
 and notifies the user (this information is also shown with <buffy-list>, which is bound to '.' per default).

## sendmail

1. **sendmail is an
MTA.** The internet's most commonly used is (was?) sendmail - see <http://www.sendmail.org/>.  
1. **sendmail is
/usr/sbin/sendmail.** All MTAs support the "sendmail" interface, that is, they provide  
 /usr/sbin/sendmail (historically /usr/lib/sendmail) and support options like -t, -f, and -bs.  
 (Mutt's $sendmail variable defaults to "/usr/sbin/sendmail -oem -oi", which should work with most MTAs like sendmail, postfix, exim, etc.)

-----

## The Bottom Line

* It is usually wrong to set MAILDIR=~/Maildir in procmail or folder=~/Maildir in Mutt (use ~/Mail).  
* mbox_type=maildir does not convert existing folders to [MaildirFormat](MaildirFormat), it only creates new ones that way.  
* Setting $MAILDIR in procmail will neither make the folder use [MaildirFormat](MaildirFormat) (this is done by the trailing slash "/")  
  nor is this a recommended location for the inbox folder (this is what $DEFAULT is for).   
* Use "fldr/" in procmail to write to a maildir folder. Do not use "fldr/new/".
